import csv
import json
import os
# first, listing all files with .csv extension
for root, directories, filenames in os.walk('.'):
    for filename in filenames:
        if filename.endswith(".csv"):
            file_path = os.path.join(root, filename)
            openFile = open(file_path, 'rU',encoding='latin-1')
            #file open! Let's separate into files...
            reader = csv.DictReader(openFile, fieldnames=("Cups", "Fecha de la lectura","Hora de la lectura","Consumo (kwh)","Clase de la lectura","Tipo Telegestión"))
            out = json.dumps([row for row in reader])
            saveFile = open(os.path.join(root,filename).split('.csv')[0]+'.json', 'w')
            saveFile.write(out)

print("All saved!")